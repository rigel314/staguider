package centroids

import (
	"image"
	"image/color"
	"image/draw"
	"math"
	"sort"
)

func Centroid(img image.Image) (ret []CentroidVal) {
	grayImage := image.NewGray(img.Bounds())
	draw.Draw(grayImage, grayImage.Bounds(), img, image.Point{}, draw.Src)

	threshold := findMedian(grayImage) * 10

	_ = findBrightSpotsInGrayImageAndZeroizeBG(grayImage, threshold)

	lbls, max := connectedComponentLabeling(grayImage)

	for lbl := 1; lbl < max; lbl++ {
		pxls := findPixelsWithLabel(lbls, lbl)
		if len(pxls) == 0 {
			continue
		}
		centroid := computeCentroid(grayImage, pxls)
		ret = append(ret, centroid)
	}

	sort.Slice(ret, func(i, j int) bool {
		return ret[i].Val > ret[j].Val
	})

	return
}

func findBrightSpotsInGrayImageAndZeroizeBG(img *image.Gray, threshold int) []image.Point {
	// Get the dimensions of the input image
	bounds := img.Bounds()
	width, height := bounds.Dx(), bounds.Dy()

	// Create an output slice to store the bright spot coordinates
	var brightSpots []image.Point

	// Iterate over each pixel in the image
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			// Get the pixel value (brightness) at the current coordinates
			pixelValue := int(img.GrayAt(x, y).Y)

			// Check if the pixel value is above the threshold
			if pixelValue < threshold {
				brightSpots = append(brightSpots, image.Point{X: x, Y: y})
				img.Set(x, y, color.Black)
			}
		}
	}

	return brightSpots
}

func findMedian(img *image.Gray) int {
	bounds := img.Bounds()
	width, height := bounds.Dx(), bounds.Dy()

	counts := make([]uint, 256)

	// Iterate over each pixel in the image
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			counts[int(img.GrayAt(x, y).Y)]++
		}
	}

	median := calcMedianList(counts)

	return median
}

func calcMedianList(list []uint) (median int) {
	top := len(list) - 1
	i := 0
	for {
		for list[i] == 0 && i < top {
			i++
		}
		for list[top] == 0 && top > i {
			top--
		}
		if top == i {
			return i
		}
		if list[i] >= list[top] {
			list[i] -= list[top]
			list[top] = 0
		} else {
			list[top] -= list[i]
			list[i] = 0
		}
		median = i
	}
}

// Label connected components in the binary image
func connectedComponentLabeling(binaryImage *image.Gray) ([][]int, int) {
	bounds := binaryImage.Bounds()
	labels := make([][]int, bounds.Dy())
	for y := range labels {
		labels[y] = make([]int, bounds.Dx())
	}

	label := 1
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			if binaryImage.GrayAt(x, y).Y == 0 {
				continue
			}
			neighbors := []int{}
			if y > bounds.Min.Y && labels[y-1][x] != 0 {
				t := labels[y-1][x]
				_ = t
				neighbors = append(neighbors, labels[y-1][x])
			}
			if x > bounds.Min.X && labels[y][x-1] != 0 {
				t := labels[y][x-1]
				_ = t
				neighbors = append(neighbors, labels[y][x-1])
			}
			if len(neighbors) == 0 {
				labels[y][x] = label
				label++
			} else {
				minNeighbor := math.MaxInt64
				for _, neighbor := range neighbors {
					if neighbor > 0 && neighbor < minNeighbor {
						minNeighbor = neighbor
					}
				}
				labels[y][x] = minNeighbor
				for _, neighbor := range neighbors {
					if neighbor > 0 && neighbor != minNeighbor {
						replaceLabel(labels, neighbor, minNeighbor)
					}
				}
			}
		}
	}
	return labels, label
}

func replaceLabel(labels [][]int, from, to int) {
	for y := range labels {
		for x := range labels[y] {
			if labels[y][x] == from {
				labels[y][x] = to
			}
		}
	}
}

type CentroidVal struct {
	X, Y float64
	Val  int
}

// Compute the centroid of a set of pixels
func computeCentroid(img *image.Gray, pixels []image.Point) CentroidVal {
	var sumX, sumY, weight float64
	val := 0
	for _, p := range pixels {
		pval := img.GrayAt(p.X, p.Y).Y
		sumX += float64(p.X) * float64(pval)
		sumY += float64(p.Y) * float64(pval)
		weight += float64(pval)
		val += int(pval)
	}
	// numPixels := float64(len(pixels))
	return CentroidVal{X: sumX / weight, Y: sumY / weight, Val: val}
}

// Find all pixels in the binary image with the specified label
func findPixelsWithLabel(labels [][]int, label int) []image.Point {
	pixels := []image.Point{}
	for y := range labels {
		for x, l := range labels[y] {
			if l == label {
				pixels = append(pixels, image.Point{X: x, Y: y})
			}
		}
	}
	return pixels
}
