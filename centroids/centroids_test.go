package centroids

import (
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"
	"math/rand"
	"os"
	"testing"
)

func TestMedian(t *testing.T) {
	counts := make([]uint, 256)

	counts[0] = 5
	counts[255] = 4
	counts[100] = 3

	med := calcMedianList(counts)

	if med != 100 {
		t.Log("med (", med, ") not equal to 100")
		t.Fail()
	}

	r := rand.New(rand.NewSource(0))

	var total uint64
	for i := 0; i < len(counts); i++ {
		counts[i] = uint(r.Uint32())
		total += uint64(counts[i])
	}
	t.Log(counts)
	t.Log(total)
	t.Log(4000 * 3000)
	t.Log(total / (4000 * 3000))
	for i := 0; i < len(counts); i++ {
		counts[i] /= uint(total / (4000 * 3000))
	}

	t.Log(counts)

	med = calcMedianList(counts)
	if med != 130 {
		t.Log("rand med (", med, ") not equal to 130")
		t.Fail()
	}
}

func TestCentroids(t *testing.T) {
	// str := "testImage.jpg"
	str := "../sta/Orion_Constellation_from_Karad,_India.png"

	f, err := os.Open(str)
	if err != nil {
		t.Log("couldn't open", str, err)
		t.FailNow()
	}
	defer f.Close()

	// i, err := jpeg.Decode(f)
	i, err := png.Decode(f)
	if err != nil {
		t.Log("couldn't decode", str, err)
		t.FailNow()
	}

	size := 20
	ximg := image.NewRGBA(image.Rectangle{image.Point{0, 0}, image.Point{size, size}})
	for y := 0; y < size; y++ {
		for x := 0; x < size; x++ {
			if x == y || x == size-y || x == 0 || y == 0 || x == size-1 || y == size-1 {
				ximg.Set(x, y, color.RGBA{255, 0, 0, 255})
			} else {
				ximg.Set(x, y, color.RGBA{0, 0, 0, 0})
			}
		}
	}

	grayImage := image.NewGray(i.Bounds())
	draw.Draw(grayImage, grayImage.Bounds(), i, image.Point{}, draw.Src)

	threshold := findMedian(grayImage) * 10

	bounds := grayImage.Bounds()
	width, height := bounds.Dx(), bounds.Dy()

	// Iterate over each pixel in the image
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			if grayImage.GrayAt(x, y).Y < uint8(threshold) {
				grayImage.Set(x, y, color.Black)
			}
		}
	}
	grey, err := os.Create("greyImage.jpg")
	if err != nil {
		t.Log("couldn't open greyImage.jpg,", err)
		t.FailNow()
	}
	err = jpeg.Encode(grey, grayImage, &jpeg.Options{Quality: 85})
	if err != nil {
		t.Log("failed encode greyImage.jpg")
		t.FailNow()
	}

	irgb := image.NewRGBA(i.Bounds())
	draw.Draw(irgb, irgb.Bounds(), i, image.Point{}, draw.Src)

	cents := Centroid(i)
	t.Log(cents)

	for i, c := range cents {
		_ = i
		if i < 3 || i > 6 {
			continue
		}
		t.Log(c.X, c.Y)
		draw.Draw(irgb, image.Rectangle{image.Point{int(c.X) - size/2, int(c.Y) - size/2}, image.Point{int(c.X) + size/2, int(c.Y) + size/2}}, ximg, image.Point{}, draw.Over)
	}

	out, err := os.Create("centroidedImage.jpg")
	if err != nil {
		t.Log("couldn't open centroidedImage.jpg,", err)
		t.FailNow()
	}

	err = jpeg.Encode(out, irgb, &jpeg.Options{Quality: 85})
	if err != nil {
		t.Log("failed encode centroidedImage.jpg")
		t.FailNow()
	}
}
