package main

import (
	"bufio"
	"encoding/gob"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"math"
	"net/http"
	"os"
	"path"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

// hip_main.dat field names from https://heasarc.gsfc.nasa.gov/W3Browse/all/hipparcos.html
/*
Hipparcos   CDS Name    HEASARC Name       Description
Cat. Field

--          * New *    Name               /Catalog Designation
H0          Catalog    * Not Displayed *  /Catalogue (H=Hipparcos)
H1          HIP        HIP_Number         /Identifier (HIP number)
H2          Proxy      Prox_10asec        /Proximity flag
H3          RAhms      RA                 /RA in h m s, ICRS (J1991.25)
H4          DEdms      Dec                /Dec in deg ' ", ICRS (J1991.25)
H5          Vmag       Vmag               /Magnitude in Johnson V
H6          VarFlag    Var_Flag           /Coarse variability flag
H7          r_Vmag     Vmag_Source        /Source of magnitude
H8          RAdeg      RA_Deg             /RA in degrees (ICRS, Epoch-J1991.25)
H9          DEdeg      Dec_Deg            /Dec in degrees (ICRS, Epoch-J1991.25)
H10         AstroRef   Astrom_Ref_Dbl     /Reference flag for astrometry
H11         Plx        Parallax           /Trigonometric parallax
H12         pmRA       pm_RA              /Proper motion in RA
H13         pmDE       pm_Dec             /Proper motion in Dec
H14         e_RAdeg    RA_Error           /Standard error in RA*cos(Dec_Deg)
H15         e_DEdeg    Dec_Error          /Standard error in Dec_Deg
H16         e_Plx      Parallax_Error     /Standard error in Parallax
H17         e_pmRA     pm_RA_Error        /Standard error in pmRA
H18         e_pmDE     pm_Dec_Error       /Standard error in pmDE
H19         DE:RA      Crl_Dec_RA         /(DE over RA)xCos(delta)
H20         Plx:RA     Crl_Plx_RA         /(Plx over RA)xCos(delta)
H21         Plx:DE     Crl_Plx_Dec        /(Plx over DE)
H22         pmRA:RA    Crl_pmRA_RA        /(pmRA over RA)xCos(delta)
H23         pmRA:DE    Crl_pmRA_Dec       /(pmRA over DE)
H24         pmRA:Plx   Crl_pmRA_Plx       /(pmRA over Plx)
H25         pmDE:RA    Crl_pmDec_RA       /(pmDE over RA)xCos(delta)
H26         pmDE:DE    Crl_pmDec_Dec      /(pmDE over DE)
H27         pmDE:Plx   Crl_pmDec_Plx      /(pmDE over Plx)
H28         pmDE:pmRA  Crl_pmDec_pmRA     /(pmDE over pmRA)
H29         F1         Reject_Percent     /Percentage of rejected data
H30         F2         Quality_Fit        /Goodness-of-fit parameter
H31         ---        * Not Displayed *  /HIP number (repetition)
H32         BTmag      BT_Mag             /Mean BT magnitude
H33         e_BTmag    BT_Mag_Error       /Standard error on BTmag
H34         VTmag      VT_Mag             /Mean VT magnitude
H35         e_VTmag    VT_Mag_Error       /Standard error on VTmag
H36         m_BTmag    BT_Mag_Ref_Dbl     /Reference flag for BT and VTmag
H37         B-V        BV_Color           /Johnson BV colour
H38         e_B-V      BV_Color_Error     /Standard error on BV
H39         r_B-V      BV_Mag_Source      /Source of BV from Ground or Tycho
H40         V-I        VI_Color           /Colour index in Cousins' system
H41         e_V-I      VI_Color_Error     /Standard error on VI
H42         r_V-I      VI_Color_Source    /Source of VI
H43         CombMag    Mag_Ref_Dbl        /Flag for combined Vmag, BV, VI
H44         Hpmag      Hip_Mag            /Median magnitude in Hipparcos system
H45         e_Hpmag    Hip_Mag_Error      /Standard error on Hpmag
H46         Hpscat     Scat_Hip_Mag       /Scatter of Hpmag
H47         o_Hpmag    N_Obs_Hip_Mag      /Number of observations for Hpmag
H48         m_Hpmag    Hip_Mag_Ref_Dbl    /Reference flag for Hpmag
H49         Hpmax      Hip_Mag_Max        /Hpmag at maximum (5th percentile)
H50         HPmin      Hip_Mag_Min        /Hpmag at minimum (95th percentile)
H51         Period     Var_Period         /Variability period (days)
H52         HvarType   Hip_Var_Type       /Variability type
H53         moreVar    Var_Data_Annex     /Additional data about variability
H54         morePhoto  Var_Curv_Annex     /Light curve Annex
H55         CCDM       CCDM_Id            /CCDM identifier
H56         n_CCDM     CCDM_History       /Historical status flag
H57         Nsys       CCDM_N_Entries     /Number of entries with same CCDM
H58         Ncomp      CCDM_N_Comp        /Number of components in this entry
H59         MultFlag   Dbl_Mult_Annex     /Double and or Multiple Systems flag
H60         Source     Astrom_Mult_Source /Astrometric source flag
H61         Qual       Dbl_Soln_Qual      /Solution quality flag
H62         m_HIP      Dbl_Ref_ID         /Component identifiers
H63         theta      Dbl_Theta          /Position angle between components
H64         rho        Dbl_Rho            /Angular separation of components
H65         e_rho      Rho_Error          /Standard error of rho
H66         dHp        Diff_Hip_Mag       /Magnitude difference of components
H67         e_dHp      dHip_Mag_Error     /Standard error in dHp
H68         Survey     Survey_Star        /Flag indicating a Survey Star
H69         Chart      ID_Chart           /Identification Chart
H70         Notes      Notes              /Existence of notes
H71         HD         HD_Id              /HD number <III 135>
H72         BD         BD_Id              /Bonner DM <I 119>, <I 122>
H73         CoD        CoD_Id             /Cordoba Durchmusterung (DM) <I 114>
H74         CPD        CPD_Id             /Cape Photographic DM <I 108>
H75         (V-I)red   VI_Color_Reduct    /VI used for reductions
H76         SpType     Spect_Type         /Spectral type
H77         r_SpType   Spect_Type_Source  /Source of spectral type
--          * New *    Class              /HEASARC BROWSE classification
*/

func main() {
	var wg sync.WaitGroup
	save("ref/hip_main.dat", "https://cdsarc.cds.unistra.fr/ftp/cats/I/239/hip_main.dat", &wg)
	save("ref/hipparcos.html", "https://heasarc.gsfc.nasa.gov/W3Browse/all/hipparcos.html", &wg)
	wg.Wait()

	f, err := os.Open("ref/hip_main.dat")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	rd := bufio.NewReader(f)

	var stars []*Star

	magLimit := 6.0

	// log.Println(maglimit(1), maglimit(5), maglimit(-1), maglimit(0))
	// log.Println(maglimit(1.64), maglimit(1.69), maglimit(1.74), maglimit(2.25))
	log.Println(haversine(0, 1, 0, 2) * 180 / math.Pi)
	// os.Exit(0)

	// process rows in star catalog
	linenum := 1
	for {
		line, err := rd.ReadString('\n')
		if !errors.Is(err, io.EOF) && err != nil {
			panic(err)
		}
		if line == "" && errors.Is(err, io.EOF) {
			break
		}
		hip_record := strings.Split(line, "|")
		if len(hip_record) != 78 {
			str := ""
			if err != nil {
				str = err.Error()
			}
			println(linenum)
			panic("invalid num fields, err: " + str)
		}
		linenum++
		s := processRow(hip_record)
		if s.Mag > magLimit {
			continue
		}

		stars = append(stars, s)
	}

	log.Println("found", len(stars), "stars <= maginude", magLimit)

	t1 := time.Now()
	sort.Slice(stars, func(i, j int) bool { return stars[i].Dec < stars[j].Dec })
	sort.SliceStable(stars, func(i, j int) bool { return stars[i].RA < stars[j].RA })
	log.Println(time.Since(t1))

	var s *Star
	for _, v := range stars {
		if v.ID == 26727 {
			s = v
			break
		}
	}

	ns := nearby(stars, s, 5)
	for _, v := range ns {
		switch v.ID {
		case 26221, 25930, 26311, 26727:
			log.Println(v.ID, v.Mag)
		}
	}
	// os.Exit(0)

	var ms []map[StarTri]*NormDist
	t1 = time.Now()
	cpus := runtime.GOMAXPROCS(-1)

	// cpus = 1

	// for _, v := range stars {
	// 	if v.ID == 41674 {
	// 		x := nearby(stars, v, 5)
	// 		ss := make([]Star, len(x))
	// 		for i, v := range x {
	// 			ss[i] = *v
	// 		}
	// 		log.Println(ss)
	// 	}
	// }
	// os.Exit(1)

	total := 0
	for _, v := range stars {
		x := nearby(stars, v, 5)
		total += len(x)
	}
	log.Println(total/len(stars), "stars in avg nearby")
	// os.Exit(0)

	chunksize := len(stars) / cpus
	start := 0
	for start < len(stars) {
		m := make(map[StarTri]*NormDist)
		ms = append(ms, m)
		end := start + chunksize
		if end > len(stars) {
			end = len(stars)
		}
		chunk := stars[start:end]
		start += chunksize
		wg.Add(1)
		go func() {
			defer wg.Done()
			for _, v := range chunk {
				x := nearby(stars, v, 5)
				for i := 0; i < len(x); i++ {
					for j := i + 1; j < len(x); j++ {
						for k := j + 1; k < len(x); k++ {
							for l := k + 1; l < len(x); l++ {
								key := []*Star{x[i], x[j], x[k], x[l]}
								sort.Slice(key[:], func(i, j int) bool { return key[i].Mag < key[j].Mag })
								realkey := StarTri{key[0].ID, key[1].ID, key[2].ID, key[3].ID}

								if _, ok := m[realkey]; ok {
									continue
								}

								a := haversine(x[i].Dec, x[i].RA, x[j].Dec, x[j].RA)
								b := haversine(x[i].Dec, x[i].RA, x[k].Dec, x[k].RA)
								c := haversine(x[i].Dec, x[i].RA, x[l].Dec, x[l].RA)
								d := haversine(x[j].Dec, x[j].RA, x[k].Dec, x[k].RA)
								e := haversine(x[j].Dec, x[j].RA, x[l].Dec, x[l].RA)
								f := haversine(x[k].Dec, x[k].RA, x[l].Dec, x[l].RA)

								norm := math.Sqrt(a*a + b*b + c*c + d*d + e*e + f*f)
								r := &NormDist{a / norm, b / norm, c / norm, d / norm, e / norm, f / norm}
								sort.Slice(r[:], func(i, j int) bool { return r[i] > r[j] })

								// log.Println(a, b, c, r)
								// os.Exit(0)

								m[realkey] = r
							}
						}
					}
				}
			}
		}()
	}
	wg.Wait()

	log.Println("building full map")

	realM := make(map[StarTri]*NormDist)
	for _, v := range ms {
		for k, v := range v {
			realM[k] = v
		}
	}

	// l := 0
	// m.Range(func(key, value any) bool { l++; return true })
	log.Println(time.Since(t1), len(realM), "StarTri in map")

	log.Println(StarTri{25336, 25930, 26311, 26727})
	log.Println(realM[StarTri{25336, 25930, 26311, 26727}])

	reverseM := make(map[*NormDist]StarTri)
	for k, v := range realM {
		reverseM[v] = k
	}

	// tri, err := os.Create("StarTri.gob.gz")
	// if err != nil {
	// 	panic(err)
	// }
	// defer tri.Close()

	// gzwr := gzip.NewWriter(tri)
	// gobwr := gob.NewEncoder(gzwr)
	// err = gobwr.Encode(reverseM)
	// if err != nil {
	// 	panic(err)
	// }

	trigob, err := os.Create("StarTri.gob")
	if err != nil {
		panic(err)
	}
	defer trigob.Close()

	gobwr := gob.NewEncoder(trigob)
	err = gobwr.Encode(reverseM)
	if err != nil {
		panic(err)
	}
}

func save(dest, url string, wg *sync.WaitGroup) {
	if dest == "" {
		dest = path.Base(url)
	}
	if _, err := os.Stat(dest); err == nil || errors.Is(err, fs.ErrExist) {
		return
	}
	wg.Add(1)
	go func() {
		resp, err := http.Get(url)
		if err != nil {
			panic("get `" + url + "` failed: " + err.Error())
		}

		if dest == "" {
			dest = path.Base(url)
		} else {
			os.MkdirAll(path.Dir(dest), 0755)
		}

		f, err := os.Create(dest)
		if err != nil {
			panic("create file `" + dest + "` failed: " + err.Error())
		}

		_, err = io.Copy(f, resp.Body)
		if err != nil {
			panic("io.Copy failed: " + err.Error())
		}
		wg.Done()
	}()
}

type Star struct {
	ID      int
	Mag     float64
	RA, Dec float64
}

func processRow(r []string) *Star {
	s := &Star{}

	for i := range r {
		r[i] = strings.Trim(r[i], " ")
	}

	idx, _ := strconv.ParseUint(r[1], 10, 64)
	s.ID = int(idx)
	s.Mag, _ = strconv.ParseFloat(r[5], 64)
	s.RA, _ = strconv.ParseFloat(r[8], 64)
	s.Dec, _ = strconv.ParseFloat(r[9], 64)

	RArate, _ := strconv.ParseFloat(r[12], 64)
	DecRate, _ := strconv.ParseFloat(r[13], 64)

	jdnow := getJulianFromUnix(time.Now().Unix())
	jddiff_yr := (jdnow - 2448349.0625) / 365.25

	if s.ID == 15404 {
		log.Println(jdnow, jddiff_yr, RArate, DecRate, s.RA, hms(s.RA), s.Dec, dms(s.Dec), julianYear(time.Now().UTC())-jdnow)
		RArate = 23.574
		DecRate = -24.407
	}

	s.RA += (RArate / math.Cos(s.Dec*math.Pi/180) * jddiff_yr) / 1000 / 3600 // rates are milli-arcsec/yr, convert to deg
	s.Dec += (DecRate * jddiff_yr) / 1000 / 3600                             // rates are milli-arcsec/yr, convert to deg

	if s.ID == 15404 {
		log.Println(jdnow, jddiff_yr, RArate, DecRate, s.RA, hms(s.RA), s.Dec, dms(s.Dec))
		// os.Exit(0)
	}

	return s
}

func julianYear(t time.Time) float64 {
	y := float64(t.Year())
	m := float64(t.Month())
	d := float64(t.Day())
	h := float64(t.Hour())
	min := float64(t.Minute())
	sec := float64(t.Second()) + float64(t.Nanosecond())/1e9

	if m <= 2 {
		y -= 1
		m += 12
	}

	a := math.Floor(y / 100)
	b := 2 - a + math.Floor(a/4)

	return math.Floor(365.25*(y+4716)) + math.Floor(30.6001*(m+1)) + d + b - 1524.5 + (h+min/60+sec/3600)/24
}

type dms float64

func (d dms) String() (str string) {
	whole_d := int(d)
	str += fmt.Sprint(whole_d) + "°_"

	mn := (float64(d) - float64(whole_d)) * 60
	whole_m := int(mn)
	str += fmt.Sprint(whole_m) + "'_"

	sc := (mn - float64(whole_m)) * 60
	str += fmt.Sprintf("%.2f", sc) + `"`

	return
}

type hms float64

func (d hms) String() (str string) {
	whole_h := int(d / (360 / 24))
	str += fmt.Sprint(whole_h) + "h_"

	mn := (float64(d) - float64(whole_h)*(360/24)) * 60 / (360 / 24)
	// log.Print("(", float64(d), whole_h, float64(whole_h)*(360/24), ")")
	whole_m := int(mn)
	str += fmt.Sprint(whole_m) + "m_"

	sc := (mn - float64(whole_m)) * 60
	str += fmt.Sprintf("%.2f", sc) + `s`

	return
}

func getJulianFromUnix(unixSecs int64) float64 {
	return (float64(unixSecs) / 86400.0) + 2440587.5
}

func nearby(stars []*Star, s *Star, distdeg float64) (ret []*Star) {
	for _, v := range stars {
		// if v.RA > s.RA-distdeg && v.RA < s.RA+distdeg &&
		// 	v.Dec > s.Dec-distdeg && v.Dec < s.Dec+distdeg { //&&
		// 	//v.Mag > s.Mag-.25 && v.Mag < s.Mag+.25 {
		// 	ret = append(ret, v)
		// }
		angle := haversine(v.Dec, v.RA, s.Dec, s.RA)
		if angle*180/math.Pi < distdeg { // &&
			//v.Mag > s.Mag-maglimit(s.Mag) && v.Mag < s.Mag+maglimit(s.Mag) {
			ret = append(ret, v)
		}
	}

	return
}

type StarTri [4]int
type NormDist [6]float64

// haversine formula to calculate the great circle distance between two points on a sphere
func haversine(dec1, ra1, dec2, ra2 float64) float64 {
	deg2rad := math.Pi / 180.0
	dec1 *= deg2rad
	ra1 *= deg2rad
	dec2 *= deg2rad
	ra2 *= deg2rad

	dLat := dec2 - dec1
	dLon := ra2 - ra1

	a := math.Pow(math.Sin(dLat/2), 2) + math.Cos(dec1)*math.Cos(dec2)*math.Pow(math.Sin(dLon/2), 2)
	// c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	c := 2 * math.Asin(math.Sqrt(a))

	return c
}

func maglimit(mag float64) float64 {
	m := 1.2
	if mag < 0 {
		return m
	}
	return m * math.Pow(m, -mag)
}
