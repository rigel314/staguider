package sta

import (
	"bytes"
	_ "embed"
	"encoding/gob"
	"log"
	"math"
	"sort"

	"gitlab.com/rigel314/staguider/centroids"
)

//go:generate go run gitlab.com/rigel314/staguider/cmd/gen/genCatalog

//go:embed StarTri.gob
var startri []byte

type StarTri [4]int
type NormDist [6]float64

var lookup map[*NormDist]StarTri

func InitializeLookup() {
	brd := bytes.NewReader(startri)
	// gzrd, err := gzip.NewReader(brd)
	// if err != nil {
	// 	panic(err)
	// }
	// gobrd := gob.NewDecoder(gzrd)
	gobrd := gob.NewDecoder(brd)
	err := gobrd.Decode(&lookup)
	if err != nil {
		panic(err)
	}
}

func Lookup(cents []centroids.CentroidVal) []int {
	ret := make([]int, len(cents))
	for i := 0; i < len(cents); i++ {
		ret[i] = -1
	}

	if len(cents) > 10 {
		cents = cents[:10]
	}
	// outer:
	for i := 0; i < len(cents); i++ {
		for j := i + 1; j < len(cents); j++ {
			for k := j + 1; k < len(cents); k++ {
				for l := k + 1; l < len(cents); l++ {
					a := dist2D(cents[i].X, cents[i].Y, cents[j].X, cents[j].Y)
					b := dist2D(cents[i].X, cents[i].Y, cents[k].X, cents[k].Y)
					c := dist2D(cents[i].X, cents[i].Y, cents[l].X, cents[l].Y)
					d := dist2D(cents[j].X, cents[j].Y, cents[k].X, cents[k].Y)
					e := dist2D(cents[j].X, cents[j].Y, cents[l].X, cents[l].Y)
					f := dist2D(cents[k].X, cents[k].Y, cents[l].X, cents[l].Y)

					norm := math.Sqrt(a*a + b*b + c*c + d*d + e*e + f*f)
					r := NormDist{a / norm, b / norm, c / norm, d / norm, e / norm, f / norm}
					// rorig := r
					sort.Slice(r[:], func(i, j int) bool { return r[i] > r[j] })

					id, fit, iddists := nearest(r)
					if fit < .0005 {
						log.Println("tried", i, j, k, l, fit, id, r, iddists, a, b, c, d, e, f)
						// // map r back to pre sorted r
						// var r2rorig []int
						// for _, rorigval := range rorig {
						// 	for j, rval := range r {
						// 		if rorigval == rval {
						// 			r2rorig = append(r2rorig, j)
						// 		}
						// 	}
						// }

						// ij := iddists[r2rorig[0]]
						// ik := iddists[r2rorig[1]]
						// il := iddists[r2rorig[2]]
						// jk := iddists[r2rorig[3]]
						// jl := iddists[r2rorig[4]]
						// kl := iddists[r2rorig[5]]

						ret[i] = id[0]
						ret[j] = id[1]
						ret[k] = id[2]
						ret[l] = id[3]
						// break outer
					}
				}
			}
		}
	}

	return ret
}

func dist2D(x1, y1, x2, y2 float64) float64 {
	return math.Sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))
}

func nearest(r NormDist) (id StarTri, fit float64, iddists *NormDist) {
	fit = math.Inf(1)
	for k, v := range lookup {
		thisfit := math.Sqrt((r[0]-k[0])*(r[0]-k[0]) + (r[1]-k[1])*(r[1]-k[1]) + (r[2]-k[2])*(r[2]-k[2]) + (r[3]-k[3])*(r[3]-k[3]) + (r[4]-k[4])*(r[4]-k[4]) + (r[5]-k[5])*(r[5]-k[5]))
		// if v[0] == 25930 && v[1] == 26220 && v[2] == 26311 && v[3] == 26727 {
		// 	log.Println(thisfit, k, v)
		// }
		if thisfit < fit {
			fit = thisfit
			id = v
			iddists = k
		}
	}

	return
}

func dist1D(a, b float64) float64 {
	return math.Abs(a - b)
}
