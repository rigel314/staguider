package sta_test

import (
	"image/png"
	"os"
	"testing"

	"gitlab.com/rigel314/staguider/centroids"
	"gitlab.com/rigel314/staguider/sta"
)

func TestLookup(t *testing.T) {
	sta.InitializeLookup()

	// str := "../centroids/testImage.jpg"
	str := "Orion_Constellation_from_Karad,_India.png"
	f, err := os.Open(str)
	if err != nil {
		t.Log("couldn't open", str, err)
		t.FailNow()
	}
	defer f.Close()

	// i, err := jpeg.Decode(f)
	i, err := png.Decode(f)
	if err != nil {
		t.Log("couldn't decode", str, err)
		t.FailNow()
	}

	cents := centroids.Centroid(i)

	t.Log(cents)

	ids := sta.Lookup(cents)
	t.Log(ids)
	t.Fail()
}
